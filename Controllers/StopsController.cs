using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TheWorld.Models;
using Microsoft.Extensions.Logging;
using AutoMapper;
using TheWorld.Services;
using Microsoft.AspNetCore.Authorization;

namespace TheWorld.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("/api/trips/{tripName}/stops")]
    public class StopsController : Controller
    {
        private readonly WorldContext _context;
        private ILogger<StopsController> _logger;
        private IWorldRepository _repository;
        private GeoCoordsService _coordsService;

        public StopsController(WorldContext context, IWorldRepository repository, ILogger<StopsController> logger, GeoCoordsService coordsService )
        {
            _context = context;
            _logger = logger;
            _repository = repository;
            _coordsService = coordsService;
        }
        
        // GET: api/trips/{tripName}/stops
        [HttpGet("")]
        public IActionResult GetStops([FromRoute] string tripName)
        {
            try
            {
                var trips = _repository.GetTripsByUsername(User.Identity.Name);
                
                return Ok(Mapper.Map<IEnumerable<StopViewModel>>(trips.Select(a=>a.Stops.OrderBy(o=> o.Order).ToList())));

            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get stops {0}", ex);                    
            }

            return BadRequest("Failed to get stops.");
        }

        /*
        // GET: api/Stops/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetStop([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var stop = await _context.Stops.SingleOrDefaultAsync(m => m.Id == id);

            if (stop == null)
            {
                return NotFound();
            }

            return Ok(stop);
        }*/
        

        // PUT: api/Stops/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStop([FromRoute] int id, [FromBody] Stop stop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stop.Id)
            {
                return BadRequest();
            }

            _context.Entry(stop).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StopExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Stops
        [HttpPost]
        public async Task<IActionResult> PostStop([FromBody] Stop stop)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _coordsService.GetCoordsAsync(stop.Name);

                    if (result.Success)
                    {
                        _logger.LogError(result.Message);
                    }
                    else {

                        stop.Latitude = result.Latitude;
                        stop.Longitude = result.Longitude;

                        _repository.AddStop(stop.Name, stop);

                        if (await _repository.SaveAsync())
                        {
                            return CreatedAtAction("GetStop", new { id = stop.Id }, stop);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("Failed to save new stop {0}", ex);
            }
            
            return BadRequest("Failed to save new stop");
        }

        // DELETE: api/Stops/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStop([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var stop = await _context.Stops.SingleOrDefaultAsync(m => m.Id == id);
            if (stop == null)
            {
                return NotFound();
            }

            _context.Stops.Remove(stop);
            await _context.SaveChangesAsync();

            return Ok(stop);
        }

        private bool StopExists(int id)
        {
            return _context.Stops.Any(e => e.Id == id);
        }
    }
}