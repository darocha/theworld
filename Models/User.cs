﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace TheWorld.Models
{
    public class User : IdentityUser
    {
        public DateTimeOffset FirstTrip { get; set; }
        
    }
}