﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheWorld.Models
{
    public class WorldRepository : IWorldRepository
    {
        private WorldContext _context;
        private ILogger<WorldRepository> _logger;

        public WorldRepository(WorldContext context, ILogger<WorldRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void AddStop(string tripName, Stop newStop)
        {
            var trip = GetTripByName(tripName);
            if (trip != null)
            {
                trip.Stops.Add(newStop);
                _context.Stops.Add(newStop);
            }
        }

        public void AddTrip(Trip newTrip)
        {
            _context.Trips.Add(newTrip);
        }

        public IEnumerable<Trip> GetAllTrips()
        {
            _logger.LogInformation("Getting all trips from the database");

            return _context.Trips.ToList();
        }

        public Trip GetTripByName(string tripName)
        {
            var trip = _context.Trips
                .Include(a=>a.Stops)
                .Where(a => a.Name == tripName)
                .SingleOrDefault();

            return trip;
        }

        public IEnumerable<Trip> GetTripsByUsername(string Username)
        {
            var trips = _context.Trips
                .Include(a => a.Stops)
                .Where(a => a.Name == Username).ToList();

            return trips;
            
        }

        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}
