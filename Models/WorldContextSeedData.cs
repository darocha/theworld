﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheWorld.Models
{
    public class WorldContextSeedData
    {
        private WorldContext _context;
        private UserManager<User> _userManager;

        public WorldContextSeedData(WorldContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureSeedData()
        {
            if(await _userManager.FindByEmailAsync("marcelo@darocha.com") == null)
            {
                var user = new User()
                {
                    UserName = "marcelo",
                    Email = "marcelo@darocha.com"

                };

                await _userManager.CreateAsync(user, "Password1!");
            }

           


            if (!_context.Trips.Any())
            {

                var usTrip = new Trip()
                {
                    DateCreated = DateTimeOffset.UtcNow,
                    Name = "US Trip",
                    UserName = "marcelo",
                    Stops = new List<Stop>()
                    {

                    }
                };

                _context.Trips.Add(usTrip);

                _context.Stops.AddRange(usTrip.Stops);


                var worldTrip = new Trip()
                {
                    DateCreated = DateTimeOffset.UtcNow,
                    Name = "World Trip",
                    UserName = "marcelo",
                    Stops = new List<Stop>()
                    {

                    }
                };

                _context.Trips.Add(worldTrip);

                _context.Stops.AddRange(worldTrip.Stops);

                await _context.SaveChangesAsync();

            }
        }
    }
}
