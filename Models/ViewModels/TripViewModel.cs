﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheWorld.Models
{
    public class TripViewModel
    {
        public string Name { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
        
}
