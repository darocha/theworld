﻿(function () {


    var sidebarbtn = $('#sidebarbtn');
    var $icon = $('.togglemenuicon', sidebarbtn);

    sidebarbtn.on('click', function () {

        $('#sidebar, #wrapper').toggleClass('hide-sidebar');
        
        if ($('#sidebar').hasClass('hide-sidebar')) {
            $icon.removeClass('fa-angle-left');
            $icon.addClass('fa-angle-right');
        }
        else {
            $icon.addClass('fa-angle-left');
            $icon.removeClass('fa-angle-right');
        }

    });

    
})();