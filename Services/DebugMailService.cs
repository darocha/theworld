﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace TheWorld.Services
{
    public class DebugMailService : IMailService
    {
        public void SendMail(string From, string To, string Subject, string Body)
        {
            Debug.WriteLine($"Sending Mail: To: {To} from: {From} subject:{Subject} Body: {Body}");
        }
    }
}
